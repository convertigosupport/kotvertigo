package com.convertigo.kotvertigo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.convertigo.clientsdk.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    var output : TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        output = findViewById(R.id.output)
    }

    fun onBt1(view: View) {
        output?.text = "run"
        val lastProgress = longArrayOf(0)
        val c8o = C8o(view.context, "https://co8test.edf.com/convertigo/projects/Pedro")
        c8o.callJson(".CheckVersion", "currentVersion", "1.0.0").thenUI { response, params ->
            output?.append("\nversion: " + response.toString(2))
            c8o.callJson("fs://pedro_hydraulic_wrench_fullsync.sync")
        }.thenUI { response, params ->
            output?.append("\npedro_hydraulic_wrench_fullsync finished !")
            c8o.callJson("fs://pedro_tolerance_gap_fullsync.sync")
        }.thenUI { response, params ->
            output?.append("\npedro_tolerance_gap_fullsync finished !")
            c8o.callJson("fs://pedro_tolerance_precision_fullsync.sync")
        }.thenUI { response, params ->
            output?.append("\npedro_tolerance_precision_fullsync finished !")
            c8o.callJson("fs://pedro_sealing_ringr_fullsync.sync")
        }.thenUI { response, params ->
            output?.append("\npedro_sealing_ringr_fullsync finished !")
            c8o.callJson("fs://pedro_thread_threadedrod_fullsync.sync")
        }.thenUI { response, params ->
            output?.append("\npedro_thread_threadedrod_fullsync finished !")
            null
        }.progress { c8oProgress ->
            val now = System.currentTimeMillis()
            if (now > lastProgress[0]) {
                c8o.runUI { output?.append("\nprogress : $c8oProgress") }
                lastProgress[0] = now + 2000
            }
        }.failUI { throwable, parameters ->
            output?.append("\nfailed $throwable")
            throwable.printStackTrace()
        }
    }

    fun onBt2(view: View) {
        output?.text = "reset !"
        val c8o = C8o(view.context, "https://co8test.edf.com/convertigo/projects/Pedro")
        c8o.callJson("fs://pedro_hydraulic_wrench_fullsync.reset").sync()
        c8o.callJson("fs://pedro_tolerance_gap_fullsync.reset").sync()
        c8o.callJson("fs://pedro_tolerance_precision_fullsync.reset").sync()
        c8o.callJson("fs://pedro_sealing_ringr_fullsync.reset").sync()
        c8o.callJson("fs://pedro_thread_threadedrod_fullsync.reset").sync()
    }
}
